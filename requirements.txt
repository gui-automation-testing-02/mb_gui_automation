behave==1.2.6
selenium==4.1.0
chromedriver-autoinstaller==0.2.2
allure-behave==2.9.45
requests==2.26.0
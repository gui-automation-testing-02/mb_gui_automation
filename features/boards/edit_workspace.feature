#Feature: Edit an existing workspace
#
#    Scenario: Verify it is possible to edit a existing workspace
#        Given the user is logged in
#        When the user clicks in Workspaces
#            And click in the workspace named "new workspace"
#            And click in Edit Workspace details
#            And edits the workspace with the following parameters:
#                | name        | type        | description                       |
#                | Marketing   | Marketing   | Now this is a marketing workspace |
#            And click in Save
#        Then the user should see the workspace modified with the previous details

import time

from pages.base_page import BasePage


class WorkspacePage(BasePage):
    def click_create_workspace(self):
        create_workspace_btn = self._webdriver.find_by_xpath('//button[@data-test-id="header-create-team-button"]')
        create_workspace_btn.click()

    def workspace_title(self, title):
        title_input = self._webdriver.find_by_xpath('//input[@data-test-id="header-create-team-name-input"]')
        title_input.send_keys(title)

    def select_workspace_type(self, type):
        type_select = self._webdriver.find_by_xpath('//div[@data-test-id="header-create-team-type-input"]')
        type_select.click()

        new_opt = self._webdriver.find_by_xpath(f'//div[contains(@id, "react-select")]/li[text()="{type}"]')
        new_opt.click()

    def click_continue(self):
        continue_btn = self._webdriver.find_by_xpath('//button[@data-test-id="header-create-team-submit-button"]')
        continue_btn.click()

    def click_later_button(self):
        later_btn = self._webdriver.find_by_xpath('//a[@data-test-id="show-later-button"]')
        later_btn.click()

    def get_workspace_title(self):
        tittle = self._webdriver.find_by_xpath(f'//div[@class="tabbed-pane-header-details"]/div/div/div/h1')
        time.sleep(5)
        return tittle.text

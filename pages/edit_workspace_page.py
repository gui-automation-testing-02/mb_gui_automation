from pages.base_page import BasePage


class EditWorkspacePage(BasePage):

    def click_workspaces(self):
        click_workspaces_btn = self._webdriver.find_by_xpath('//button[@data-test-id="workspace-switcher"]')
        click_workspaces_btn.click()

    def select_workspace(self, workspace_name):
        select_workspace = self._webdriver.find_by_xpath(f'//div[@data-test-id="workspace-list"]/a/p[text()="{workspace_name}"]')
        select_workspace.click()

    def click_edit_workspace_details(self):
        click_edit_workspace_details_btn = self._webdriver.find_by_xpath('//div[@class="js-current-details"]/div/button')
        click_edit_workspace_details_btn.click()

    def edit_workspace_details(self, title, type, description):
        # Edit the title of the workspace
        types_workspace_name_input = self._webdriver.find_by_id('displayName')
        types_workspace_name_input.clear()
        types_workspace_name_input.send_keys(title)

        # Edit the type of the workspace
        type_select = self._webdriver.find_by_xpath('//form[@id="organization-detail-form"]/div/div/div/div')
        type_select.click()
        new_opt = self._webdriver.find_by_xpath(f'//div[contains(@id, "react-select")]/li[text()="{type}"]')
        new_opt.click()

        # Edit the description of the workspace
        types_workspace_name_input = self._webdriver.find_by_id('desc')
        types_workspace_name_input.send_keys(description)

    def click_save(self):
        click_save_btn = self._webdriver.find_by_xpath('//button[text()="Save"]')
        click_save_btn.click()

    def verify_workspace_details(self, title, type, description):
        # Verify the title of the workspace
        types_workspace_name_input = self._webdriver.find_by_xpath(f'//div[@class="tabbed-pane-header-details"]/div/div/div/h1')
        assert types_workspace_name_input.text == title

        # Verify the description of the workspace
        types_workspace_name_input = self._webdriver.find_by_xpath(f'//div[@class="js-current-details"]/div/div/div/p[text()="{description}"]')
        assert types_workspace_name_input.text == description
    
        # Verify the type of the workspace
        click_edit_workspace_details_btn = self._webdriver.find_by_xpath('//div[@class="js-current-details"]/div/button')
        click_edit_workspace_details_btn.click()

        type_select = self._webdriver.find_by_xpath('//form[@id="organization-detail-form"]/div/div/div/div')
        assert type_select.text == type

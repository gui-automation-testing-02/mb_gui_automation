from behave import *
from pages.login_page import LoginPage


@given('the user is logged in')
def login_to_trello(context):
    login_page = LoginPage(context.webdriver)
    login_page.navidate_to_login(context.webapp_base_url)
    login_page.set_email(context.webapp_email)
    login_page.click_login_with_atlassian()
    login_page.set_password(context.webapp_password)
    login_page.click_login()

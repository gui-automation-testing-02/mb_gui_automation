from behave import *

from pages.edit_workspace_page import EditWorkspacePage

use_step_matcher('re')


@step('the user clicks in Workspaces')
def click_workspaces(context):
    workspace_page = EditWorkspacePage(context.webdriver)
    workspace_page.click_workspaces()


@step('(?:the user )?click in the workspace named "(?P<workspace_name>.*?)"')
def select_workspace(context, workspace_name):
    workspace_page = EditWorkspacePage(context.webdriver)
    workspace_page.select_workspace(workspace_name)


@step('(?:the user )?click in Edit Workspace details')
def click_edit_workspace_details(context):
    workspace_page = EditWorkspacePage(context.webdriver)
    workspace_page.click_edit_workspace_details()


@step('(?:the user )?edits the workspace with the following parameters')
def edit_workspace_details(context):
    workspace_page = EditWorkspacePage(context.webdriver)
    context.table_workspace_details = context.table
    for row in context.table:
        workspace_page.edit_workspace_details(row['name'], row['type'], row['description'])


@step('(?:the user )?click in Save')
def click_save(context):
    workspace_page = EditWorkspacePage(context.webdriver)
    workspace_page.click_save()


@step('(?:the user )?should see the workspace modified with the previous details')
def verify_workspace_details(context):
    workspace_page = EditWorkspacePage(context.webdriver)
    table = context.table_workspace_details
    for row in table:
        workspace_page.verify_workspace_details(row['name'], row['type'], row['description'])

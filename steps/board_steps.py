from behave import *
from helpers import context_helper, requests_helper
from pages.board_page import BoardsPage

use_step_matcher('re')


@step('the user clicks Create')
def click_create(context):
    board_page = BoardsPage(context.webdriver)
    board_page.click_create()


@step('(?:the user )?clicks Create Board')
def click_create_board(context):
    board_page = BoardsPage(context.webdriver)
    board_page.click_create_board()


@step('(?:the user )?types "(?P<title>.*?)" in board title(?: as "(?P<variable>.*?)")?')
def set_title(context, title, variable):
    title = context_helper.replace_all(title, context)
    board_page = BoardsPage(context.webdriver)
    board_page.set_title(title)
    if variable is not None:
        setattr(context, variable, title)


@step('(?:the user )?selects "(?P<option>.*?)" in Visibility')
def select_visibility(context, option):
    board_page = BoardsPage(context.webdriver)
    board_page.select_visibility(option)


@step('(?:the user )?clicks Create on Create board popover')
def click_create_on_popover(context):
    board_page = BoardsPage(context.webdriver)
    board_page.click_create_on_popover()


@then('(?:the user )?should see "(?P<expected_title>.*?)" in board header')
def get_board_tittle(context, expected_title):
    expected_title = context_helper.replace_all(expected_title, context)
    board_page = BoardsPage(context.webdriver)
    actual_title = board_page.get_board_tittle()
    assert expected_title == actual_title


@step('(?:the user )?should see the board "(?P<title>.*?)" from the REST API')
def verify_board(context, title):
    title = context_helper.replace_all(title, context)
    url = context.restapi_base_url + '/members/me/boards'
    response = requests_helper.get_request(url, context.params)
    response_as_dict = response.json()
    for item in response_as_dict:
        if item['name'] == title:
            for (key, expected_value) in context.table:
                actual_value = requests_helper.get_value_from_dict(key, item)
                assert actual_value == expected_value
            break


@step('(?:the user )?deletes the board "(?P<title>.*?)" from the REST API')
def delete_board(context, title):
    title = context_helper.replace_all(title, context)
    url = context.restapi_base_url + '/members/me/boards'
    response = requests_helper.get_request(url, context.params)
    response_as_dict = response.json()
    for item in response_as_dict:
        if item['name'] == title:
            board_id = item['id']
            url = context.restapi_base_url + '/boards/' + board_id
            requests_helper.delete_request(url, context.params)

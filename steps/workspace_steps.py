from behave import *

from pages.workspace_page import WorkspacePage

use_step_matcher('re')


@step('(?:the user )?clicks Create Workspace')
def click_create_workspace(context):
    workspace_page = WorkspacePage(context.webdriver)
    workspace_page.click_create_workspace()


@step('(?:the user )?types "(?P<title>.*?)" in Workspace title')
def workspace_title(context, title):
    workspace_page = WorkspacePage(context.webdriver)
    workspace_page.workspace_title(title)


@step('(?:the user )?selects "(?P<type>.*?)" in Workspace type')
def select_workspace_type(context, type):
    workspace_page = WorkspacePage(context.webdriver)
    workspace_page.select_workspace_type(type)


@step('(?:the user )?clicks Continue')
def click_continue(context):
    workspace_page = WorkspacePage(context.webdriver)
    workspace_page.click_continue()


@step("(?:the user )?clicks I'll do this later button")
def click_later_button(context):
    workspace_page = WorkspacePage(context.webdriver)
    workspace_page.click_later_button()


@then('(?:the user )?the user should see "(?P<expected_title>.*?)" in the workspace header')
def get_workspace_title(context, expected_title):
    workspace_page = WorkspacePage(context.webdriver)
    actual_title = workspace_page.get_workspace_title()
    assert expected_title == actual_title

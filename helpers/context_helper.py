import random
import re
import string

regex = '{{(.*?)}}'
default_length = 10


def replace_all(text, context):
    text = replace_keywords(text)
    text = replace_variable(text, context)
    return text


def replace_keywords(text):
    keywords = re.findall(regex, text)
    for keyword in keywords:
        match keyword:
            case 'random_string':
                random_string = get_random_string(default_length)
                text = text.replace('{{' + keyword + '}}', random_string, 1)
            case 'random_number':
                print('Not implemented yet')

    return text


def get_random_string(length):
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


def replace_variable(text, context):
    varaibles = re.findall(regex, text)
    for variable in varaibles:
        value = getattr(context, variable, None)
        if value is not None:
            text = text.replace('{{' + variable + '}}', value)
    return text
